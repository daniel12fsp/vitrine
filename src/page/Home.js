import React from "react";
import { Component } from "react";
import requestHelper from "../helper/requestHelper";
import Showcase from "../component/Showcase";
import "./reset.css";

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: "loading",
      products: [],
      error: null
    };
  }

  async componentDidMount() {
    try {
      const products = await requestHelper();
      this.setState({ status: "ready", products });
    } catch (error) {
      this.setState({ status: "error", error });
    }
  }

  render() {
    const { products, error, status } = this.state;
    switch (status) {
      case "loading":
        return <div> It's loading </div>;
      case "ready":
        return <Showcase products={products} />;
      case "error":
        return <div> It's a error, {error} </div>;
      default:
        throw new Error("Invalid Status", status);
    }
  }
}
