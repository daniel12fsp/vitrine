import "@babel/polyfill"; // add support for async/await syntax for old browsers
import React from 'react';
import ReactDOM from 'react-dom';
import Home from "./page/Home";

ReactDOM.render(<Home />, document.getElementById('root'));