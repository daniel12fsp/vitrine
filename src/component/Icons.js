import React, {Component} from "react";
import nextVitrineDisableIcon from "../../assets/nextVitrineDisable.png";
import nextVitrineEnableIcon from "../../assets/nextVitrineEnable.png";

import previousVitrineDisable from "../../assets/previousVitrineDisable.png";
import previousVitrineEnable from "../../assets/previousVitrineEnable.png";

import styles from "./Icons.css";
const icons = {
  previous(enable) {
    return enable ? previousVitrineEnable : previousVitrineDisable;
  },
  next(enable) {
    return enable ? nextVitrineEnableIcon : nextVitrineDisableIcon;
  }
};

export default class Icons extends Component {
  handleHover = status => {
    const { onHover } = this.props;
    if (status) {
      onHover();
      this.hover = setTimeout(this.handleHover, 200, status);
    } else {
      clearTimeout(this.hover);
    }
  };

  render() {
    const { type, className, enable } = this.props;
    return (
      <div className={className}>
        <img
          onMouseOver={() => this.handleHover(true)}
          onMouseOut={() => this.handleHover(false)}
          className={styles.img}
          src={icons[type](enable)}
        />
      </div>
    );
  }
}
