import React, { Component } from "react";
import styles from "./Showcase.css";
import Product from "./Product";
import Icons from "./Icons";
const IMG_AMOUNT_SCROLL = 30;
export default class Showcase extends Component {
  constructor(props) {
    super(props);
    this.state = {
      enablePrevBtn: false,
      enableNextBtn: true
    };
    this.widthScroll = 0;
  }

  componentDidUpdate(){
    this.widthScroll = this.productsContainer.scrollWidth - this.productsContainer.clientWidth;
  }

  updateIcons = () => {
    if (this.productsContainer) {

      this.setState({
        enablePrevBtn: this.productsContainer.scrollLeft > 0,
        enableNextBtn: this.productsContainer.scrollLeft < this.widthScroll
      });
    }
  };
  
  handleClickPrevBtn = () => {
    if (this.productsContainer) {
      this.productsContainer.scrollLeft -= IMG_AMOUNT_SCROLL;
      this.updateIcons();
    }
  };

  handleClickNextBtn = () => {
    if (this.productsContainer) {
      this.productsContainer.scrollLeft += IMG_AMOUNT_SCROLL;
      this.updateIcons();
    }
  };

  render() {
    const { enablePrevBtn, enableNextBtn } = this.state;
    const { products } = this.props;
    const mainProduct = products.data.reference.item;
    const recomendationsProducts = products.data.recommendation.map(product => (
      <Product key={product.businessId} product={product} />
    ));
    return (
      <div className={styles.container}>
        <div className={styles.mainProduct}>
          <div className={styles.title}>Você visitou:</div>
          <Product product={mainProduct} />
        </div>
        <div className={styles.recommendationProductsCol}>
          <div className={styles.title}>e talvez se interesse por:</div>
          <div className={styles.recommendationList}>
            <div className={styles.separator} />
            <Icons
              onHover={this.handleClickPrevBtn}
              className={styles.icons}
              type="previous"
              enable={enablePrevBtn}
            />
            <div
              className={styles.productsContainer}
              ref={elem => (this.productsContainer = elem)}
            >
              {recomendationsProducts}
            </div>
            <Icons
              onHover={this.handleClickNextBtn}
              className={styles.icons}
              type="next"
              enable={enableNextBtn}
            />
          </div>
        </div>
      </div>
    );
  }
}
