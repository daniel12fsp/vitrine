import React from "react";
import styles from "./Product.css";
import Dotdotdot from "react-dotdotdot";

export default function Product({product}) {
  const {
    businessId,
    name,
    detailUrl,
    imageName,
    price,
    oldPrice,
    productInfo: {paymentConditions}
  } = product;
  //TODO problably, the best way to resolve this is change on banckend.
  const paymentConditionsJSX = paymentConditions.replace(
    /(\d+\.\d+)/,
    match => `R$ ${match.replace(".", ",")}`
  );

  return (
    <a key={businessId} className={styles.resetAnchor} href={detailUrl}>
      <div className={styles.container}>
        <div className={styles.containerImg}>
          <img src={imageName} className={styles.img} />
        </div>
        <div className={styles.containerText}>
          <div className={styles.name}>
            <Dotdotdot clamp={3}>{name}</Dotdotdot>
          </div>
          {oldPrice && (
            <div className={styles.oldPrice}>
              De: <span>{price}</span>
            </div>
          )}
          <div className={styles.containerPrice}>
            Por: <span className={styles.price}>{price}</span>
            <div
              className={styles.paymentConditions}
              dangerouslySetInnerHTML={{ __html: paymentConditionsJSX }}
            />
          </div>
        </div>
      </div>
    </a>
  );
}
