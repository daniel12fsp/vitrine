const defaultURL = process.env.GIT_LAB ? "challenge.json": "https://secret-ocean-49799.herokuapp.com/http://roberval.chaordicsystems.com/challenge/challenge.json?callback=X";

export default function requestHelper(url = defaultURL) {
  return new Promise((resolve, reject) => {
    window.X = resolve;
    const script = document.createElement("script");
    script.onerror = reject;
    script.src = url;
    document.body.appendChild(script);
  });
}
