const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const path = require("path");
const webpack = require("webpack");

const GIT_LAB =
  process.env && process.env.GIT_LAB && JSON.parse(process.env.GIT_LAB);

function createPluginArr(mode) {
  const plugins = [
    new webpack.DefinePlugin({
      "process.env.GIT_LAB": GIT_LAB
    }),
    new MiniCssExtractPlugin({
      filename: "styles.css",
      chunkFilename: "[id].css"
    }),
    new HtmlWebpackPlugin({
      template: "src/index.html",
      minify: {
        collapseWhitespace: true
      }
    })
  ];

  if (GIT_LAB) {
    plugins.push(new CopyWebpackPlugin(["./assets/challenge.json"]));
  }

  if (mode === "production") {
    plugins.push(
      new MiniCssExtractPlugin({
        filename: "styles.css"
      })
    );
  }
  return plugins;
}

module.exports = (_, { mode }) => ({
  output: {
    filename: "bundle.js",
    chunkFilename: "[name].bundle.js",
    path: path.join(__dirname, "dist")
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.css$/,
        use: [
          mode === "production" ? MiniCssExtractPlugin.loader: "style-loader",
          {
            loader: "css-loader",
            options: {
              modules: true,
              localIdentName: "[name]__[local]--[hash:base64:5]"
            }
          }
        ]
      },
      {
        test: /\.(png|jpg|gif)$/i,
        use: [
          {
            loader: "url-loader"
          }
        ]
      }
    ]
  },
  devtool: mode === "production" ? undefined : "cheap-module-eval-source-map",
  optimization: {
    splitChunks: {
      chunks: "all"
    },
    minimizer:
      mode === "production" ? [new OptimizeCSSAssetsPlugin({}),new UglifyJsPlugin()] : undefined
  },
  plugins: createPluginArr()
});
