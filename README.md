# Package used

- **husky** - to trigger prettier to format code before commit.
- **prettier** and **pretty-quick** - to format code.
- **webpack**  - to bundle the code.
- **react** - to create UI components.
- **react-dotdotdot** - to handle multline ellipsis.
- **babel** - to use the next javascript and have some syntactic sugar ;).

# How to access online

> I had some problems because I hosted in gitlab page(only **https** ) which only host sites in https request. Another problem, It was modern browsers reject http request when a site is https. So, I decided to host in two way: in [**the same folder**](https://daniel12fsp.gitlab.io/vitrine/allgitlab/) and [**bypass cross request limitations**](https://daniel12fsp.gitlab.io/vitrine/allhttps/).

## 1. Go to https://daniel12fsp.gitlab.io/vitrine/allgitlab/

How to build to using request gitlab

```bash
yarn install
yarn prod:gitlab
```

All request are in gitlab resource and challenge.json

## 2. Go to https://daniel12fsp.gitlab.io/vitrine/allhttps/

How to build to bypassing request *Context Mixed* https policy

```bash
yarn install
yarn prod
```